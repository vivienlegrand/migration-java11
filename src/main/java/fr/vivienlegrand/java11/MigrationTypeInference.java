package fr.vivienlegrand.java11;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MigrationTypeInference {

    private String whatever;

    // Standard Object

    public MigrationTypeInference historicalObject() {
        MigrationTypeInference object = new MigrationTypeInference();
        object.setWhatever("whatever");
        return object;
    }

    public MigrationTypeInference withJava11Object() {
        var object = new MigrationTypeInference();
        object.setWhatever("whatever");
        return object;
    }

    // With genericity

    public List<Integer> historicalGenericity() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        return list;
    }

    public List<Integer> withJava11Genericity() {
        var list = new ArrayList<Integer>();
        list.add(1);
        return list;
    }
}
