package fr.vivienlegrand.java11;

public interface MigrationInterface {

    // Private methods in interface

    private <T extends Number> boolean canDivide(T denominator) {
        return !denominator.equals(0);
    }

    default Integer division(int numerator, int denominator) {
        if (canDivide(denominator)) {
            return numerator / denominator;
        }

        return null;
    }

    default double division(double numerator, double denominator) {
        if (canDivide(denominator)) {
            return numerator / denominator;
        }

        return Double.NaN;
    }
}
