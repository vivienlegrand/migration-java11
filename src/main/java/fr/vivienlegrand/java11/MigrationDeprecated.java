package fr.vivienlegrand.java11;

public class MigrationDeprecated {

    @Deprecated
    public void historical() {
    }

    @Deprecated(since = "0.9", forRemoval = true)
    public void withJava11() {
    }
}
