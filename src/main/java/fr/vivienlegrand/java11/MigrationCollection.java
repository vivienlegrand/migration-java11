package fr.vivienlegrand.java11;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MigrationCollection {

    public Set<Integer> historicalWay() {
        Set<Integer> set = new HashSet<>();

        set.add(1);
        set.add(2);

        return set;
    }

    public Set<Integer> withArrays() {
        return new HashSet<>(
                Arrays.asList(1, 2)
        );
    }

    public Set<Integer> withJava11() {
        return Set.of(1, 2);
    }

}
