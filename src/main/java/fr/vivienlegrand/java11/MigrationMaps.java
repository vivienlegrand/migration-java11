package fr.vivienlegrand.java11;

import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;

public class MigrationMaps {

    public Map<Integer, String> historicalWay() {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "value1");
        map.put(2, "value2");

        return map;
    }

    public Map<Integer, String> withStaticInitialization() {
        return new HashMap<>() {{
            put(1, "value1");
            put(2, "value2");
        }};
    }

    public Map<Integer, String> withGuava() {
        return ImmutableMap.of(
                1, "value1",
                2, "value2"
        );
    }

    public Map<Integer, String> withJava11() {
        return Map.of(
                1, "value1",
                2, "value2"
        );
    }
}
