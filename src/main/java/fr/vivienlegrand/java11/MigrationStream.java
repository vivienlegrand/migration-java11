package fr.vivienlegrand.java11;

import java.util.stream.Stream;

public class MigrationStream {

    public Stream<Integer> newMethods() {
        return Stream.iterate(-10, i -> i < 20, i -> ++i)
                .takeWhile(i -> i > 0)
                .dropWhile(i -> i > 2);
    }
}
