package fr.vivienlegrand.java11;

import java.util.Optional;
import java.util.stream.Stream;

public class MigrationOptional {

    public char[] newMethodOr() {
        return optionalRandomGenerator()
                .or(MigrationOptional::optionalRandomGenerator)
                .map(randomNumber -> randomNumber.toString().toCharArray())
                .orElse(new char[0]);
    }

    public void newMethodIfPresentOrElse() {
        optionalRandomGenerator().ifPresentOrElse(
                this::doStuffIfPresent,
                this::doStuffIfAbscent
        );
    }

    public Stream<Double> newMethodStream() {
        return optionalRandomGenerator().stream();
    }

    public boolean newMethodIsEmpty() {
        return optionalRandomGenerator().isEmpty();
    }

    private void doStuffIfPresent(Double number) {
    }

    private void doStuffIfAbscent() {
    }

    private static Optional<Double> optionalRandomGenerator() {
        double random = Math.random();

        if (random == 0.) {
            return Optional.empty();
        } else {
            return Optional.of(random);
        }
    }
}
