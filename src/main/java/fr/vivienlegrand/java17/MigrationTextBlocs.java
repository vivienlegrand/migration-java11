package fr.vivienlegrand.java17;

public class MigrationTextBlocs {

    public String historicalWay() {
        return "Vivien Le Grand\n"
                + "90 route de Mittelhausbergen\n"
                + "67200 Strasbourg\n"
                + "            FRANCE";
    }

    public String withTextBlocs() {
        return """
                Vivien Le Grand
                90 route de Mittelhausbergen
                67200 Strasbourg
                            FRANCE""";
    }
}
