package fr.vivienlegrand.java17;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MigrationStreamToList {
    public void sinceJava8() {
        List<Integer> list = Stream.of(1, 2, 3).collect(Collectors.toList());
    }

    public void sinceJava17() {
        List<Integer> list = Stream.of(1, 2, 3).toList();
    }
}
