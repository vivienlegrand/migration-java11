package fr.vivienlegrand.java17;

public class MigrationSwitch {

    public int historicalWay(String query) {
        int result;
        switch (query) {
            case "Result 1":
            case "Result a":
                result = 1;
                break;
            case "Result 2":
            case "Result b":
                result = 2;
                break;
            default:
                result = 0;
                break;
        }
        return result;
    }

    public int sinceJava17(String query) {
        return switch (query) {
            case "Result 1", "Result a" -> 1;
            case "Result 2", "Result b" -> 2;
            default -> 0;
        };
    }
}
