package fr.vivienlegrand.java17;

public class MigrationInstanceOf {

    public void historicalWay() {
        Vehicle vehicle = new Car();

        if (vehicle instanceof Car) {
            Car car = (Car) vehicle;

            if (car.hasGas()) {
                car.startEngine();
            }
        }
    }

    public void withJava17() {
        Vehicle vehicle = new Car();

        if (vehicle instanceof Car car && car.hasGas()) {
            car.startEngine();
        }
    }

    public interface Vehicle {
        int wheelsNumber();
    }

    public class Car implements Vehicle {
        @Override
        public int wheelsNumber() {
            return 4;
        }

        public void startEngine() {
        }

        public boolean hasGas() {
            return true;
        }
    }

    public class Bike implements Vehicle {
        @Override
        public int wheelsNumber() {
            return 2;
        }
    }
}
