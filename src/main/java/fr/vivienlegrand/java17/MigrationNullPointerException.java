package fr.vivienlegrand.java17;

public class MigrationNullPointerException {

    // JEP 358 : Better logs for the NullPointerException

    // Previous :
    //  Exception in thread "main" java.lang.NullPointerException
    //  at fr.vivienlegrand.java17.MigrationNullPointerException.main(MigrationNullPointerException.java:18)

    // Now :
    // Exception in thread "main" java.lang.NullPointerException: Cannot invoke "java.lang.Integer.intValue()" because "array[0]" is null
    // at fr.vivienlegrand.java17.MigrationNullPointerException.main(MigrationNullPointerException.java:18)

    public static void main(String[] args) {
        Integer[] array = new Integer[1];
        int i = 2;

        int y = i * array[0];
    }
}
