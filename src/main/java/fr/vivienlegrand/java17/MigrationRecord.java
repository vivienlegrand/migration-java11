package fr.vivienlegrand.java17;

public class MigrationRecord {

    public class Dto1 {
        private int whatever;

        public int getWhatever() {
            return whatever;
        }

        public void setWhatever(int whatever) {
            this.whatever = whatever;
        }
    }

    public record Dto2(int whatever) {
    }

    public void initDtos() {
        Dto1 dto1 = new Dto1();
        dto1.setWhatever(1);
        dto1.getWhatever();

        Dto2 dto2 = new Dto2(2);
        dto2.whatever();
    }
}
