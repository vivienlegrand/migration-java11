package fr.vivienlegrand.java17;

public class MigrationSealedClasses {

    public sealed class A permits B1, C1 {
    }

    public sealed class B1 extends A permits B2 {
    }

    public final class B2 extends B1 {
    }

    public non-sealed class C1 extends A {
    }

    public class C2 extends C1 {
    }
}
