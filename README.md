# Nouveautés de Java 11

## Améliorations au niveau de la JVM
* Modularisation
* Garbage Collector
* String
* JShell

## Nouveaux APIs
* Deprecated
    * since
    * forRemovale
* Collections
    * Initialisation des Maps, Set et List
* Stream
    * takeWile
    * dropWile
* Optional
    * ifPresentOrElse
    * or
* Interfaces
    * private methods
* Type Inference (var)
